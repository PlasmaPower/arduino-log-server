var send = require('koa-send');

module.exports = function (router) {
  router.get('/', function (ctx, next) {
    return send(ctx, '/root.html', {
      root: __base + '/static'
    });
  });
};
