var send = require('koa-send');

module.exports = function (router) {
  router.get('/interface/:arduino', function (ctx, next) {
    return send(ctx, '/interface.html', {
      root: __base + '/static'
    });
  });
};
