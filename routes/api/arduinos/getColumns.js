module.exports = function (router) {
  router.get('/:arduinoId/getColumns', function (ctx, next) {
    return ctx.models.Arduino.findById(ctx.params.arduinoId, {
      include: [ctx.models.Column]
    }).then(function (arduino) {
      if (!arduino) {
        return ctx.throw(404);
      }
      ctx.body = arduino.dataValues.Columns.map(function (column) {
        return column.dataValues;
      });
    });
  });
};
