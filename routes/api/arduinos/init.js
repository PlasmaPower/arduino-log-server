module.exports = function (router) {
  router.get('/:arduinoName/init', function (ctx, next) {
    if (!ctx.authenticated) {
      return ctx.throw(401);
    }
    return ctx.models.Arduino.findOrCreate({
      where: {
        internalName: ctx.params.arduinoName
      },
      defaults: {
        internalName: ctx.params.arduinoName,
        externalName: ctx.params.arduinoName
      }
    }).then(arduinos => ctx.body = arduinos[0].id);
  });
};
