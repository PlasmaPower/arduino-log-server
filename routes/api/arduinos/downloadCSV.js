var dataToCSV = require(__base + '/utils/dataToCSV.js');

module.exports = function (router) {
  router.get('/:arduinoId/downloadCSV', function (ctx) {
    return ctx.models.Arduino.findById(ctx.params.arduinoId).then(function (arduino) {
      if (!arduino) {
        return ctx.throw(404);
      }
      ctx.set('Content-Disposition', 'attachment; filename=' +
          arduino.internalName.match(/[a-zA-Z0-9]/g).join('') + '.csv');
      var queryKeys = Object.keys(ctx.query);
      var customHeaders = ctx.query.customHeaders !== undefined;
      var dataPromisesPromise = arduino.getRows({
        order: [['unixTime', 'ASC']]
      }).then(function (rows) {
        return rows.map(function (row) {
          return row.getColumnValues({
            include: [ctx.models.Column]
          }).then(values => ({ row, values }));
        });
      });
      var queryPromise = Promise.resolve().then(function () {
        if (customHeaders) {
          var UTCOffset = 0;
          var columns = [];
          for (var i = 0; i < queryKeys.length; i++) {
            var queryKey = queryKeys[i];
            if (queryKey.indexOf('header_') === 0) {
              columns.push({
                name: ctx.query[queryKey],
                contents: {
                  type: 'column',
                  internalName: queryKey.substring('header_'.length)
                }
              });
            } else if (customHeaders && queryKey === 'timestamp') {
              columns.push({
                name: ctx.query[queryKey],
                contents: {
                  type: 'timestamp'
                }
              });
            } else if (queryKey === 'UTCOffset') {
              UTCOffset = ctx.query.UTCOffset;
            }
          }
          return { columns, UTCOffset };
        } else {
          var UTCOffset = ctx.query.UTCOffset || 0;
          var columns = [
            {
              name: 'Timestamp',
              contents: {
                type: 'timestamp'
              }
            }
          ];
          return arduino.getColumns().then(function (dbColumns) {
            var lastColumnIndex = dbColumns.length - 1;
            dbColumns.forEach(function (column, index) {
              columns[column.internalName] = column.externalName;
              columns.push({
                name: column.externalName,
                contents: {
                  type: 'column',
                  internalName: column.internalName
                }
              });
            });
            return { columns, UTCOffset };
          });
        }
      });
      return Promise.all([dataPromisesPromise, queryPromise]).then(function (things) {
        var dataPromises = things[0]; // TODO: destructuring
        var query = things[1];
        ctx.body = dataToCSV(dataPromises, query.columns, query.UTCOffset);
      });
    });
  });
};
