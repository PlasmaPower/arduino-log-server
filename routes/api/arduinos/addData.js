module.exports = function (router) {
  router.post('/:arduinoName/addData', function (ctx) {
    if (!ctx.authenticated) {
      return ctx.throw(401);
    }
    ctx.body = '';
    var time = ctx.query.timestamp;
    if (isNaN(time)) {
      time = Date.parse(time);
    } else {
      time = parseInt(time);
    }
    var arduinoPromise = ctx.models.Arduino.find({
      where: {
        internalName: ctx.params.arduinoName
      }
    }).then(function (arduino) {
      if (!arduino) {
        return ctx.throw(404);
      }
      return arduino;
    });
    var rowPromise = ctx.models.Row.create({
      unixTime: time || Date.now()
    });
    var columnValuePromises = Object.keys(ctx.request.body).map(function (internalHeader) {
      return ctx.models.ColumnValue.create({
        value: parseFloat(ctx.request.body[internalHeader])
      }).then(columnValue => ({internalHeader, columnValue}));
    });
    return Promise.all([
      Promise.all([arduinoPromise, rowPromise]).then(function (things) {
        var arduino = things[0]; // TODO destructuring
        var row = things[1];
        arduino.addRow(row);
      }),
      Promise.all([
        arduinoPromise, rowPromise, Promise.all(columnValuePromises)
      ]).then(function (things) {
        var arduino = things[0]; // TODO destructuring
        var row = things[1];
        var columnValues = things[2];
        return Promise.all(columnValues.map(function (column) {
          var columnValue = column.columnValue; // TODO destructuring
          var internalHeader = column.internalHeader;
          return Promise.all([
            row.addColumnValue(columnValue),
            columnValue.setRow(row),
            ctx.models.Column.findOne({
              where: {
                internalName: internalHeader,
                ArduinoId: arduino.id
              },
              lock: ctx.transaction.LOCK.UPDATE
            }).then(function (column) {
              return column || ctx.models.Column.create({
                internalName: internalHeader,
                externalName: internalHeader,
                color: Math.floor(Math.random() * 16777216),
                ArduinoId: arduino.id
              });
            }).then(function (column) {
              return Promise.all([
                column.addColumnValue(columnValue),
                columnValue.setColumn(column)
              ]);
            })
          ]);
        }));
      })
    ]);
  });
};
