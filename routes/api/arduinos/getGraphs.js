module.exports = function (router) {
  router.get('/:arduinoId/getGraphs', function (ctx, next) {
    return ctx.models.Arduino.findById(ctx.params.arduinoId, {
      include: [{
        model: ctx.models.Graph,
        include: [ctx.models.GraphComponent]
      }]
    }).then(function (arduino) {
      if (!arduino) {
        return ctx.throw(404);
      }
      ctx.body = arduino.dataValues.Graphs.map(function (graph) {
        var data = graph.dataValues;
        data.components = data.GraphComponents.map(function (component) {
          return component.dataValues;
        });
        delete data.GraphComponents;
        return data;
      });
    });
  });
};
