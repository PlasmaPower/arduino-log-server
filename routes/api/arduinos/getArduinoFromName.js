module.exports = function (router) {
  router.get('/getArduinoFromName/:arduinoName', function (ctx, next) {
    return ctx.models.Arduino.findOne({
      where: {
        internalName: ctx.params.arduinoName
      }
    }).then(function (arduino) {
      if (!arduino) {
        return ctx.throw(404);
      }
      ctx.body = arduino.dataValues;
    });
  });
};
