module.exports = function (router) {
  router.get('/:arduinoId/setExternalNameTo/:newArduinoName', function (ctx, next) {
    if (!ctx.authenticated) {
      return ctx.throw(401);
    }
    ctx.body = '';
    return ctx.models.Arduino.findById(ctx.params.arduinoId).then(function (arduino) {
      if (!arduino) {
        return ctx.throw(404);
      }
      arduino.set('externalName', ctx.params.newArduinoName);
      return arduino.save();
    });
  });
};
