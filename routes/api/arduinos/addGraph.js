module.exports = function (router) {
  router.post('/:arduinoId/addGraph', function (ctx) {
    if (!ctx.authenticated) {
      return ctx.throw(401);
    }
    var body = ctx.request.body;
    ctx.assert(body.externalName, 400, 'No external name given');
    ctx.assert(body.type, 400, 'No type given');
    return ctx.models.Arduino.findById(ctx.params.arduinoId).then(function (arduino) {
      ctx.assert(arduino, 404, 'Arduino not found');
      return arduino;
    }).then(function (arduino) {
      return ctx.models.Graph.create({
        externalName: body.externalName,
        type: body.type,
      }).then(function (graph) {
        ctx.assert(body.components, 400, 'No components given');
        ctx.body = graph.id;
        return Promise.all([
          graph.setArduino(arduino),
          Promise.all(body.components.map(function (component) {
            ['columnId', 'role', 'color']
              .forEach(attr => ctx.assert(component[attr], 400, 'A component had no ' + attr));
            return ctx.models.GraphComponent.create({
              ColumnId: component.columnId,
              GraphId: graph.id,
              role: component.role,
              color: component.color
            });
          }))
        ]);
      });
    });
  });
};
