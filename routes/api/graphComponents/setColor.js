module.exports = function (router) {
  router.get('/:componentId/setColorTo/:color', function (ctx) {
    if (!ctx.authenticated) {
      return ctx.throw(401);
    }
    ctx.body = '';
    return ctx.models.GraphComponent.findById(ctx.params.componentId).then(function (component) {
      component.set('color', ctx.params.color);
      return component.save();
    });
  });
};
