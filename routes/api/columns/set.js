module.exports = function (router) {
  var attributes = [{
    name: 'color',
    route: 'setColorTo'
  }, {
    name: 'externalName',
    route: 'setExternalNameTo'
  }, {
    name: 'hidden',
    route: 'setHiddenTo',
    type: Boolean
  }];
  attributes.forEach(function (attribute) {
    router.get('/:columnId/' + attribute.route + '/:newValue', function (ctx) {
      if (!ctx.authenticated) {
        return ctx.throw(401);
      }
      ctx.body = '';
      return ctx.models.Column.findById(ctx.params.columnId).then(function (column) {
        if (!column) {
          return ctx.throw(404);
        }
        var newValue = ctx.params.newValue;
        if (attribute.type === Boolean) {
          newValue = newValue === 'true';
        }
        column.set(attribute.name, newValue);
        return column.save();
      });
    });
  });
};
