module.exports = function (router) {
  router.get('/:columnId/getRecentData', function (ctx) {
    return ctx.models.Column.findById(ctx.params.columnId).then(function (column) {
      if (!column) {
        return ctx.throw(404);
      }
      return column.getColumnValues({
        include: [ctx.models.Row],
        order: [[ctx.models.Row, 'unixTime', 'DESC']],
        limit: 10
      });
    }).then(function (columnValues) {
      // Get the unixTime out of the row
      columnValues = columnValues.map(function (columnValue) {
        var value = columnValue.dataValues;
        value.timestamp = value.Row.get('unixTime');
        delete value.Row;
        return value;
      });
      // We order by desc so the limit works, but we want them asc
      columnValues.reverse();
      ctx.body = columnValues;
    });
  });
};
