var compose = require('koa-compose');
var middleware = ['authenticate', 'models', 'transaction'];
middleware = middleware.map(name => require(__base + '/middleware/' + name));

module.exports = function (router) {
  router.use(compose(middleware));
};
