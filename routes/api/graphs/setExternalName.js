module.exports = function (router) {
  router.get('/:graphId/setExternalNameTo/:name', function (ctx) {
    if (!ctx.authenticated) {
      return ctx.throw(401);
    }
    ctx.body = '';
    return ctx.models.Graph.findById(ctx.params.graphId).then(function (graph) {
      graph.set('externalName', ctx.params.name);
      return graph.save();
    });
  });
};
