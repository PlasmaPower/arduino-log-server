module.exports = function (router) {
  router.get('/:graphId/delete', function (ctx) {
    if (!ctx.authenticated) {
      return ctx.throw(401);
    }
    ctx.body = '';
    return ctx.models.Graph.findById(ctx.params.graphId).then(function (graph) {
      return graph.destroy();
    });
  });
};
