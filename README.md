# Arduino Log Server

This stores and manages data. It is not specific to Arduinos, but was originally made for them. The server is written in Node.JS using Koa, and the web interface uses Angular.

## Example Use

Startup the server: `bin/www`

Initialize an Arduino: `GET /api/arduinos/example/init`

Add data to the Arduino

```
POST /api/arduinos/example/addData

header=value&otherHeader=otherValue
```

Note that any requests that modify data, such as `init` and `addData`, must supply the header `X-Password` to match with what is defined in the config.

View the data through a browser: `/interface/example`

Download a CSV of the data: `GET /api/arduinos/1/downloadCSV`

1 is the ID of the first Arduino. You can get this link from the interface. This also supports custom formats with the query string. Pass UTCOffset to set the utc offset of the wanted time zone in milliseconds. If you set customHeaders to anything, then you can specify which headers you would like to output. You can do this by setting `header_exampleHeaderName=headerNameInCSV`. You can also use `timestamp=timestampHeaderInCSV` which will give you an Excel compatible timestamp in your CSV (done by default if you don't have custom headers enabled).
