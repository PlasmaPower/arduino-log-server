Chart.defaults.Line.bezierCurve = false;
Chart.defaults.Line.animation = false;

angular.module('DataView', ['chart.js', 'colorpicker.module'])

.controller('BodyController', function ($scope, $http) {
  $scope.name = decodeURIComponent(window.location.pathname.split('/')[2]);
  $scope.loadingData = true;
  $scope.dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  $scope.arduino = { externalName: $scope.name };
  $scope.UTCOffset = new Date().getTimezoneOffset() * 60 * 1000;
  $scope.saveArduinoName = function () {
    var url = '/api/arduinos/' + $scope.arduino.id;
    url += '/setExternalNameTo/' + encodeURIComponent($scope.arduino.externalName);
    $http.get(url).then(function () {
      $scope.arduino.savedExternalName = $scope.arduino.externalName;
    }).catch(function (res) {
      if (res.status === 401) {
        alert(res.data);
      }
    });
  };
  $scope.saveColumnName = function (column) {
    var url = '/api/columns/' + column.id + '/setExternalNameTo/' + encodeURIComponent(column.externalName);
    $http.get(url).then(function () {
      column.savedExternalName = column.externalName;
    }).catch(function (res) {
      if (res.status === 401) {
        alert(res.data);
      }
    });
  };
  $scope.saveColumnColor = function (column) {
    var url = '/api/columns/' + column.id + '/setColorTo/' + parseInt(column.color.substring(1), 16);
    $http.get(url).then(function () {
      column.savedColor = column.color;
    }).catch(function (res) {
      if (res.status === 401) {
        alert(res.data);
      }
    });
  };
  $scope.setColumnHidden = function (column, hiddenValue) {
    var url = '/api/columns/' + column.id + '/setHiddenTo/' + hiddenValue.toString();
    $http.get(url).then(function () {
      column.hidden = hiddenValue;
    }).catch(function (res) {
      if (res.status === 401) {
        alert(res.data);
      }
    });
  };
  $http.get('/api/arduinos/getArduinoFromName/' + encodeURIComponent($scope.name)).then(function (res) {
    var arduino = res.data;
    arduino.savedExternalName = arduino.externalName;
    $scope.arduino = arduino;
    return $http.get('/api/arduinos/' + arduino.id + '/getColumns');
  }).then(function (res) {
    var columns = res.data;
    $scope.columns = columns;
    return Promise.all(columns.map(function (column) {
      return $http.get('/api/columns/' + column.id + '/getRecentData').then(function (res) {
        var recentData = res.data;
        column.recentData = recentData;
        column.savedExternalName = column.externalName;
        column.graphValues = [recentData.map(function (val) {
          return val.value;
        })];
        column.graphLabels = recentData.map(function (val) {
          var date = new Date(val.timestamp);
          return $scope.dayNames[date.getDay()] + ' ' + date.getHours() + ':' + date.getMinutes();
        });
        column.color = column.color.toString(16);
        if (column.color.length < 6) {
          column.color = new Array(7 - column.color.length).join('0') + column.color;
        }
        column.savedColor = column.color = '#' + column.color;
      });
    }));
  }).then(function () {
    $scope.loadingData = false;
  }).catch(function (res) {
    $scope.loadingData = false;
    $scope.loadingError = res.status;
  });
  $scope.password = localStorage.getItem('password');
  $scope.$watch('password', function () {
    if ($scope.password) {
      localStorage.setItem('password', $scope.password);
    } else {
      localStorage.removeItem('password');
    }
    $http.defaults.headers.common = {
      'X-Password': $scope.password
    };
  });
  $http.defaults.headers.common = {
    'X-Password': $scope.password
  };
});
