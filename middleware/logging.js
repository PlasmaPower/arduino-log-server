var config = require('../config.js');
var logger = require('koa-logger');

module.exports = function (ctx, next) {
  if (config.logging) {
    return logger(config.logging)(ctx, next);
  } else {
    return next();
  }
}
