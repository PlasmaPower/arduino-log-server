var models = require(__base + '/models');

module.exports = (ctx, next) => {
  ctx.models = models;
  return next();
};
