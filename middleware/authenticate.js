var config = require(__base + '/config.js');

module.exports = function (ctx, next) {
  Object.defineProperty(ctx, 'authenticated', {
    get: function () {
      // The complexity of this code is to prevent timing attacks
      var password = ctx.get('X-Password');
      var configPassword = config.password;
      if (!password || !configPassword) {
        return false;
      }
      password = password.toString();
      configPassword = configPassword.toString();
      var invalidLength = password.length !== configPassword.length;
      if (invalidLength) {
        configPassword = password;
      }
      var result = 0;
      for (var i = 0; i < password.length; i++) {
        result |= password.charCodeAt(i) ^ configPassword.charCodeAt(i);
      }

      if (invalidLength) {
        return false;
      } else {
        return result === 0;
      }
    }
  });
  return next();
};
