module.exports = function (ctx, next) {
  return ctx.models.sequelize.transaction().then(function (transaction) {
    ctx.transaction = transaction;
    ctx.models.sequelizeCLS.set('transaction', transaction);
    return next().then(function () {
      return transaction.commit();
    }).catch(function (err) {
      return transaction.rollback().then(() => { throw err; });
    });
  });
};
