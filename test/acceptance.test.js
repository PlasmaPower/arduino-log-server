process.env.NODE_ENV = 'testing';
var assert = require('assert');
var request = require('supertest');
var mysql = require('mysql');
var async = require('async');
var fs = require('fs');
var path = require('path');
var config = require('../config.js');

var app;

var arduino, columns, graphs;

before(function (done) {
  this.timeout(5000);
  if (config.database.dialect !== 'sqlite') {
    var connection = mysql.createConnection({
      host: config.database.host,
      user: config.database.username,
      password: config.database.password
    });
    connection.connect();
    connection.query('DROP DATABASE IF EXISTS arduinoLogServerTest', function (err) {
      if (err) {
        return done(err);
      }
      connection.query('CREATE DATABASE arduinoLogServerTest', function (err) {
        if (err) {
          return done(err);
        }
        var server = require('../app.js');
        app = server.listen();
        server.dbReady.then(function () {
          done();
        });
      });
    });
  } else {
    var server = require('../app.js');
    app = server.listen();
    server.dbReady.then(function () {
      done();
    });
  }
});

describe('static', function () {
  describe('/interface/:id', function () {
    it('gives HTML', function (done) {
      this.slow(100);
      request(app)
        .get('/interface/:id')
        .expect('Content-Type', /html/)
        .expect(200, done);
    });
  });

  describe('/', function () {
    it('gives HTML', function (done) {
      request(app)
        .get('/')
        .expect('Content-Type', /html/)
        .expect(200, done);
    });
  });
});

describe('arduinos', function () {
  var id;

  describe('/api/arduinos/:name/init', function (done) {
    it('successfully initializes an arduino and gives back it\'s id', function (done) {
      this.slow(200);
      request(app)
        .get('/api/arduinos/example/init')
        .set('X-Password', '1234')
        .expect(200, function (err, res) {
          if (err) done(err);
          id = parseInt(res.body);
          assert(id);
          done();
        });
    });

    it('gives a 401 without a password', function (done) {
      request(app)
        .get('/api/arduinos/anotherArduino/init')
        .expect(401, done);
    });
  });

  describe('/api/arduinos/:id/setExternalNameTo/:name', function (done) {
    it('will set the external name of an arduino', function (done) {
      request(app)
        .get('/api/arduinos/' + id + '/setExternalNameTo/Example External Name')
        .set('X-Password', '1234')
        .expect(200, done);
    });

    it('gives a 404 for an unknown id', function (done) {
      request(app)
        .get('/api/arduinos/100/setExternalNameTo/Example External Name')
        .set('X-Password', '1234')
        .expect(404, done);
    });

    it('gives a 401 without a password', function (done) {
      request(app)
        .get('/api/arduinos/' + id + '/setExternalNameTo/Another External Name')
        .expect(401, done);
    });
  });

  describe('/api/arduinos/getArduinoFromName/:name', function (done) {
    it('gives back the arduino information', function (done) {
      request(app)
        .get('/api/arduinos/getArduinoFromName/example')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          var data = res.body;
          arduino = data;
          assert.ok(data.id, 'arduino has id');
          assert.equal(data.internalName, 'example', 'arduino has correct internalName');
          assert.equal(data.externalName, 'Example External Name', 'arduino has correct externalName');
          done();
        });
    });

    it('gives a 404 for an unknown id', function (done) {
      request(app)
        .get('/api/arduinos/getArduinoFromName/99999')
        .expect(404, done);
    });
  });
});

describe('data', function () {
  describe('/api/arduinos/:name/addData', function () {
    it('successfully adds data to an arduino', function (done) {
      this.slow(1000);
      this.timeout(5000);
      request(app)
        .post('/api/arduinos/example/addData')
        .set('X-Password', '1234')
        .send({
          "header',0": 2,
          "header',1": 2,
          "header',2": 2,
          "header',3": 2,
          "header',4": 2
        })
        .expect(200, done);
    });

    it('successfully fills in data for an arduino', function (done) {
      this.slow(1000);
      this.timeout(5000);
      request(app)
        .post('/api/arduinos/example/addData?timestamp=10')
        .set('X-Password', '1234')
        .send({
          "header',0": 1,
          "header',1": 1,
          "header',2": 1,
          "header',3": 1,
          "header',4": 1
        })
        .expect(200, done);
    });

    it('gives a 401 without a password', function (done) {
      request(app)
        .post('/api/arduinos/example/addData')
        .send({
          header1: 5,
          header2: 5
        })
        .expect(401, done);
    });

    it('gives a 404 for an unknown id', function (done) {
      request(app)
        .post('/api/arduinos/unknown/addData')
        .set('X-Password', '1234')
        .send({
          header1: 5,
          header2: 5
        })
        .expect(404, done);
    });
  });

  describe('/api/arduinos/:id/getColumns', function () {
    it('gives back valid columns', function (done) {
      request(app)
        .get('/api/arduinos/' + arduino.id + '/getColumns')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          var data = res.body;
          columns = data;
          assert.equal(data.length, 5, 'arduino has 5 columns');
          for (var i = 0; i < 5; i++) {
            var column = data[i];
            assert.ok(column, 'column is truthy');
            assert.ok(column.id, 'columns has an id');
            assert.ok(column.color, 'column has a color');
            assert(!column.hidden, 'column is not hidden');
            assert.equal(column.internalName, 'header\',' + i, 'column has correct internalName');
            assert.equal(column.externalName, 'header\',' + i, 'column has correct externalName');
          }
          done();
        });
    });

    it('gives a 404 for an unknown id', function (done) {
      request(app)
        .get('/api/arduinos/100/getColumns')
        .expect(404, done);
    });
  });

  describe('/columns/:id/getRecentData', function () {
    it('gives back valid first values', function (done) {
      request(app)
        .get('/api/columns/' + columns[0].id + '/getRecentData')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          var data = res.body;
          assert.equal(data.length, 2, 'column has 2 data points');
          for (var i = 0; i < 2; i++) {
            var point = data[i];
            assert.equal(point.value, i + 1, 'data point is correct');
            if (i === 0) {
              assert.equal(point.timestamp, 10, 'data point has correct timestamp');
            } else {
              assert.ok(point.timestamp, 'data point has timestamp');
            }
          }
          done();
        });
    });

    it('gives back a 404 for an unknown id', function (done) {
      request(app)
        .get('/api/columns/100/getRecentData')
        .expect(404, done)
    });
  });

  describe('/api/arduinos/:id/downloadCSV', function () {
    this.slow(100);

    it('gives a valid CSV', function (done) {
      this.slow(200);
      request(app)
        .get('/api/arduinos/' + arduino.id + '/downloadCSV')
        .expect('Content-Disposition', /^attachment; filename=.*\.csv$/)
        .expect(200, function (err, res) {
          if (err) {
            done(err);
          }
          var csv = res.text.split('\n');
          assert.equal(csv[0],
              "Timestamp,'header'',0','header'',1','header'',2','header'',3','header'',4'",
              'CSV has correct header');
          for (var i = 1; i <= 2; i++) {
            if (i === 1) {
              assert.equal(csv[i], '1970-1-1 0:0:0,1,1,1,1,1', 'CSV has correct line');
            }
            assert(!!csv[i].match(new RegExp('\\d\\d\\d\\d-\\d\\d?-\\d\\d? \\d\\d?:\\d\\d?:\\d\\d?,(' + i + ',){4}' + i)),
                'CSV has valid line');
          }
          done();
        });
    });

    it('gives a valid CSV when columns are specified', function (done) {
      request(app)
        .get('/api/arduinos/' + arduino.id + '/downloadCSV' +
            "?customHeaders&UTCOffset=10&timestamp=time&header_header',1=first&header_header',2=second")
        .expect(200, function (err, res) {
          if (err) {
            done(err);
          }
          var csv = res.text.split('\n');
          assert.equal(csv[0], 'time,first,second', 'CSV has correct header');
          for (var i = 1; i <= 2; i++) {
            if (i === 1) {
              assert.equal(csv[i], '1970-1-1 0:0:0,1,1', 'CSV has correct line');
            }
            assert(!!csv[i].match(new RegExp('\\d\\d\\d\\d-\\d\\d?-\\d\\d? \\d\\d?:\\d\\d?:\\d\\d?,' + i + ',' + i)),
                'CSV has valid line');
          }
          done();
        });
    });

    it('gives a valid CSV when no columns are specified', function (done) {
      request(app)
        .get('/api/arduinos/' + arduino.id + '/downloadCSV?customHeaders&UTCOffset=10')
        .expect(200, function (err, res) {
          if (err) {
            done(err);
          }
          var csv = res.text.split('\n');
          assert.equal(csv[0], '', 'CSV has correct header');
          for (var i = 1; i <= 2; i++) {
            assert.equal(csv[i], '', 'CSV has correct line');
          }
          done();
        });
    });

    it('gives a 404 for an unknown id', function (done) {
      request(app)
        .get('/api/arduinos/100/downloadCSV' +
            "?customHeaders&UTCOffset=10&timestamp=time&header_header',1=first&header_header',2=second")
        .expect(404, done);
    });
  });
});

describe('graphs', function (done) {
  describe('/api/arduinos/:id/addGraph', function () {
    it('successfully adds graphs for an arduino', function (done) {
      this.slow(200);
      async.forEachSeries([0, 1], function (number, callback) {
        request(app)
          .post('/api/arduinos/' + arduino.id + '/addGraph')
          .set('X-Password', '1234')
          .send({
            externalName: 'graph' + number,
            type: 'typeExample',
            components: [
              {columnId: columns[0].id, role: 'role0', color: 5},
              {columnId: columns[1].id, role: 'role1', color: 6}
            ]
          })
          .expect(200, function (err, res) {
            if (err) callback(err);
            assert(parseInt(res.body));
            callback();
          });
      }, done);
    });

    it('gives a 401 without a password', function (done) {
      request(app)
        .post('/api/arduinos/' + arduino.id + '/addGraph')
        .send({
          externalName: 'name',
          type: 'type',
          components: [
            {column: columns[0].id, role: 'role'}
          ]
        })
        .expect(401, done);
    });
  });

  describe('/api/arduinos/:id/getGraphs', function () {
    it('gives back valid graphs', function (done) {
      request(app)
        .get('/api/arduinos/' + arduino.id + '/getGraphs')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          graphs = res.body;
          assert.equal(graphs.length, 2, 'there are two graphs');
          for (var i = 0; i < graphs.length; i++) {
            var graph = graphs[i];
            assert.ok(graph.id, 'graph has an id');
            assert.equal(graph.externalName, 'graph' + i, 'graph has the correct externalName');
            assert.equal(graph.type, 'typeExample', 'graph has the correct type');
            for (var n = 0; n < graph.components.length; n++) {
              var component = graph.components[n];
              assert.ok(component, 'graph component is truthy');
              assert.equal(component.role, 'role' + n, 'graph component has correct role');
              assert.equal(component.color, 5 + n, 'graph component has correct color');
            }
          }
          done();
        });
    });

    it('gives a 404 for an unknown id', function (done) {
      request(app)
        .get('/api/arduinos/100/getGraphs')
        .expect(404, done);
    });
  });

  describe('/api/graphs/:id/delete', function () {
    it('deletes a graph', function (done) {
      request(app)
        .get('/api/graphs/' + graphs[1].id + '/delete')
        .set('X-Password', '1234')
        .expect(200, done);
    });

    it('gives a 401 without a password', function (done) {
      request(app)
        .get('/api/graphs/' + graphs[0].id + '/delete')
        .expect(401, done);
    });
  });

  describe('/api/graphs/:id/setExternalNameTo/:name', function () {
    it('changes a graph name', function (done) {
      request(app)
        .get('/api/graphs/' + graphs[0].id + '/setExternalNameTo/newGraphName')
        .set('X-Password', '1234')
        .expect(200, done);
    });

    it('gives a 401 without a password', function (done) {
      request(app)
        .get('/api/graphs/' + graphs[0].id + '/setExternalNameTo/otherGraphName')
        .expect(401, done);
    });
  });

  describe('/api/graphComponents/:id/setColorTo/:id', function () {
    it('sets the color of a component', function (done) {
      request(app)
        .get('/api/graphComponents/' + graphs[0].components[0].id + '/setColorTo/10')
        .set('X-Password', '1234')
        .expect(200, done);
    });

    it('gives a 401 without a password', function (done) {
      request(app)
        .get('/api/graphComponents/' + graphs[0].components[0].id + '/setColorTo/20')
        .expect(401, done);
    });
  });

  describe('/api/arduinos/:id/getGraphs', function () {
    it('gives back new graphs', function (done) {
      request(app)
        .get('/api/arduinos/' + arduino.id + '/getGraphs')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          graphs = res.body;
          assert.equal(graphs.length, 1, 'there is one graph');
          var graph = graphs[0];
          assert.ok(graph.id, 'graph has an id');
          assert.equal(graph.externalName, 'newGraphName', 'graph has the correct externalName');
          assert.equal(graph.type, 'typeExample', 'graph has the correct type');
          for (var i = 0; i < graph.components.length; i++) {
            var component = graph.components[i];
            assert.equal(component.role, 'role' + i, 'graph component has the correct role');
            if (i === 0) {
              assert.equal(component.color, 10, 'graph component has correct color');
            } else {
              assert.equal(component.color, 5 + i, 'graph component has correct color');
            }
          }
          done();
        });
    });
  });
});

describe('columns', function () {
  describe('/api/columns/:id/set*', function () {
    it('sets the color', function (done) {
      request(app)
        .get('/api/columns/' + columns[0].id + '/setColorTo/0')
        .set('X-Password', '1234')
        .expect(200, done);
    });

    it('sets the external name', function (done) {
      request(app)
        .get('/api/columns/' + columns[0].id + '/setExternalNameTo/exampleColumn')
        .set('X-Password', '1234')
        .expect(200, done);
    });

    it('gives a 401 setting the external name without a password', function (done) {
      request(app)
        .get('/api/columns/' + columns[0].id + '/setExternalNameTo/otherColumnName')
        .expect(401, done);
    });

    it('gives a 404 setting the external name of an unknown id', function (done) {
      request(app)
        .get('/api/columns/100/setExternalNameTo/otherColumnName')
        .set('X-Password', '1234')
        .expect(404, done);
    });

    it('sets hidden', function (done) {
      request(app)
        .get('/api/columns/' + columns[0].id + '/setHiddenTo/true')
        .set('X-Password', '1234')
        .expect(200, done);
    });
  });

  describe('/api/arduinos/:id/getColumns', function () {
    it('has changed getColumns', function (done) {
      request(app)
        .get('/api/arduinos/' + arduino.id + '/getColumns')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          var data = res.body;
          var column = data[0];
          assert.equal(column.id, columns[0].id, 'column has the correct id');
          assert.equal(column.color, 0, 'column has the correct color');
          assert.equal(column.internalName, columns[0].internalName, 'column has the correct internalName');
          assert.equal(column.externalName, 'exampleColumn', 'column has the correct externalName');
          assert(column.hidden, 'column is hidden');
          columns = data;
          done();
        });
    });
  });
});
