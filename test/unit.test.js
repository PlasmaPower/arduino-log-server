var fs = require('fs');
var path = require('path');
var assert = require('assert');

process.env.NODE_ENV = 'testing';
__base = path.join(__dirname, '..');

describe('middleware', function () {
  describe('authenticate', function (done) {
    var mw = require('../middleware/authenticate.js');
    var req = {};
    var ret = mw(req, () => 'foobar');

    it('should call return next', function () {
      assert(ret === 'foobar');
    });
    it('should return true if the passwords are equal', function () {
      req.get = () => '1234';
      assert(req.authenticated);
    });
    it('should return false if the passwords are different', function () {
      req.get = () => '1235';
      assert(!req.authenticated);
    });
    it('should return false if the passwords have different lengths', function () {
      req.get = () => '12345';
      assert(!req.authenticated);
    });
  });

  describe('logging', function (done) {
    it('should use the logger if enabled', function () {
      process.env.NODE_ENV = 'development';
      delete require.cache[require.resolve('../config.js')];
      delete require.cache[require.resolve('../middleware/logging.js')];
      var mw = require('../middleware/logging.js');
      assert(mw({}, () => new Promise(() => {})) instanceof Promise);
      process.env.NODE_ENV = 'testing';
    });
    it('should return next if not enabled', function () {
      process.env.NODE_ENV = 'testing';
      delete require.cache[require.resolve('../config.js')];
      delete require.cache[require.resolve('../middleware/logging.js')];
      var mw = require('../middleware/logging.js');
      assert.equal(mw({}, () => 'foobar'), 'foobar');
    });
  });
});

describe('config', function () {
  var configs = [];

  it('should have a config for every environment', function () {
    var passwordFile = path.join(__dirname, '../password');
    var hasPasswordFile = false;
    try {
      fs.statSync(passwordFile);
      hasPasswordFile = true;
    } catch (e) {} // Likely ENOTENT
    if (!hasPasswordFile) {
      fs.closeSync(fs.openSync(passwordFile, 'w'));
    }
    ['development', 'testing', 'production'].forEach(function (env) {
      process.env.NODE_ENV = env;
      delete require.cache[require.resolve('../config.js')];
      var config = require('../config.js');
      assert.equal(typeof config, 'object');
      configs[env] = config;
    });
    if (!hasPasswordFile) {
      fs.unlinkSync(passwordFile);
    }
  });

  it('should default to the development environment', function () {
    delete process.env.NODE_ENV;
    delete require.cache[require.resolve('../config.js')];
    assert.deepEqual(require('../config.js'), configs['development']);
    process.env.NODE_ENV = 'testing';
  });
});

describe('models', function () {
  describe('TestObject', function () {
    it('should create a test object', function () {
      var called = false;
      require('../models/test.js')({
        define: () => called = true
      });
      assert(called);
    });

    it('should not create anything if not testing', function () {
      process.env.NODE_ENV = 'development';
      delete require.cache[require.resolve('../models/test.js')];
      var called = false;
      require('../models/test.js')({
        define: () => called = true
      });
      assert(!called);
      process.env.NODE_ENV = 'testing';
    });
  });

  it('should return the models', function () {
    var models = require('../models');
    assert(models);
    assert(models.TestObject);
    assert(models.sequelize);
  });

  it('should return the models in development', function () {
    process.env.NODE_ENV = 'development';
    delete require.cache[require.resolve('../models')];
    delete require.cache[require.resolve('../models/test.js')];
    var models = require('../models');
    assert(models);
    assert(!models.TestObject);
    assert(models.sequelize);
    process.env.NODE_ENV = 'testing';
  });
});

describe('utils', function () {
  describe('dataToCSV', function () {
    var dataToCSV = require('../utils/dataToCSV.js');

    it('should error for an unknown type and properly handle the error', function (done) {
      var columns = [
        {
          name: 'columnName',
          contents: {
            type: 'unknown type',
          }
        }
      ];
      var dataPromises = [
        Promise.resolve({
          values: [],
          Column: { internalName: 'internalName' }
        })
      ];
      var stream = dataToCSV(dataPromises, columns, 0);
      var foundError = false;
      var text = '';
      stream.on('data', function (data) {
        text += data;
      });
      stream.on('end', function () {
        assert(text.includes('ERROR'));
        done();
      });
    })
  });
});
