global.__base = __dirname; // path.join would be better, but would be in every file

var Koa = require('koa');
var path = require('path');
var favicon = require('koa-favicon');
var bodyParser = require('koa-bodyparser');
var serve = require('koa-static');
var mount = require('koa-mount');
var config = require('./config.js');
var models = require('./models');

var app = new Koa();

app.dbReady = models.onReady;

app.use(bodyParser());
app.use(favicon(__dirname + '/static/favicon.png'));
app.use(mount('/static', serve(__dirname + '/static')));
app.use(require('./middleware/logging'));

app.use(require('./utils/loadRoutes'));

module.exports = app;
