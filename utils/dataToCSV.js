var Stream = require('stream');
var CSVEscape = require('./CSVEscape.js');

function excelDate(date) {
  var str = '';
  str += date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCDate();
  str += ' ';
  str += date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds();
  return str;
}

module.exports = function (dataPromises, columns, UTCOffset) {
  var stream = new Stream.PassThrough();
  stream.write(columns.map(col => col.name).map(CSVEscape).join(',') + '\n');
  dataPromises.reduce(function (promise, dataPromise) {
    return promise.then(() => dataPromise).then(function (data) {
      var columnValuesObj = {};
      data.values.forEach(function (columnValue) {
        columnValuesObj[columnValue.Column.internalName] = columnValue.value;
      });
      stream.write(columns.map(function (column) {
        var contents = column.contents;
        if (contents.type === 'column') {
          return columnValuesObj[contents.internalName];
        } else if (contents.type === 'timestamp') {
          return excelDate(new Date(data.row.unixTime - UTCOffset));
        } else {
          throw new Error('Unknown column type ' + contents.type);
        }
      }).join(','));
      stream.write('\n');
    });
  }, Promise.resolve())
    .then(() => stream.end())
    .catch(function (err) {
      console.error(err);
      stream.end('A PROCESSING ERROR OCCURRED - This data file is not complete');
    });

  return stream;
};
