var fs = require('fs');
var path = require('path');
var compose = require('koa-compose');
var mount = require('koa-mount');
var Router = require('koa-router');

var middleware = [];

function processDir(superRouter, dirs) {
  dirs = dirs || [];
  var dirPath = path.join(__base, 'routes', ...dirs);
  var files = fs.readdirSync(dirPath);
  var router = Router();
  var todoDirs = []; // Process dirs after files
  files.forEach(function (file) {
    var filePath = path.join(dirPath, file);
    var stats = fs.statSync(filePath);
    if (stats.isDirectory()) {
      todoDirs.push(file);
    } else if (stats.isFile() && file.endsWith('.js')) {
      require(filePath)(router);
    }
  });
  todoDirs.forEach(dir => processDir(router, [...dirs, dir]));
  if (superRouter && dirs.length) {
    superRouter.use('/' + encodeURIComponent(dirs[dirs.length - 1]), router.routes(), router.allowedMethods());
  }
  return router;
}

var router = processDir();

module.exports = compose([router.routes(), router.allowedMethods()]);
