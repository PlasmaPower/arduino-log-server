module.exports = function (value) {
  if (value.indexOf(',') === -1) {
    return value;
  }
  return '\'' + value.replace('\'', '\'\'') + '\'';
};
