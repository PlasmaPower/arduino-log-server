'use strict';

module.exports = function (sequelize, DataTypes) {
  var ColumnValue = sequelize.define('ColumnValue',  {
    value: DataTypes.FLOAT
  }, {
    classMethods: {
      associate: function (models) {
        ColumnValue.belongsTo(models.Column);
        ColumnValue.belongsTo(models.Row);
      }
    }
  });

  return ColumnValue;
};
