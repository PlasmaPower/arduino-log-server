'use strict';

module.exports = function (sequelize, DataTypes) {
  var Row = sequelize.define('Row', {
    unixTime: DataTypes.BIGINT
  }, {
    classMethods: {
      associate: function (models) {
        Row.hasMany(models.ColumnValue);
      }
    }
  });

  return Row;
};
