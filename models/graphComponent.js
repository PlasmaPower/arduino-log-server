'use strict';

module.exports = function (sequelize, DataTypes) {
  var GraphComponent = sequelize.define('GraphComponent',  {
    role: DataTypes.STRING,
    color: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        GraphComponent.belongsTo(models.Graph);
        GraphComponent.hasOne(models.Column);
      }
    }
  });

  return GraphComponent;
};
