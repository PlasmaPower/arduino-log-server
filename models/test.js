'use strict';

module.exports = function(sequelize, DataTypes) {
  if (process.env.NODE_ENV !== 'testing') {
    return;
  }

  var TestObject = sequelize.define('TestObject', {});

  return TestObject;
};
