'use strict';

module.exports = function (sequelize, DataTypes) {
  var Graph = sequelize.define('Graph',  {
    externalName: DataTypes.STRING,
    type: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        Graph.hasMany(models.GraphComponent);
        Graph.belongsTo(models.Arduino);
      }
    }
  });

  return Graph;
};
