'use strict';

module.exports = function(sequelize, DataTypes) {
  var Arduino = sequelize.define('Arduino', {
    internalName: DataTypes.STRING,
    externalName: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        Arduino.hasMany(models.Column);
        Arduino.hasMany(models.Row);
        Arduino.hasMany(models.Graph);
      }
    }
  });

  return Arduino;
};
