'use strict';

module.exports = function (sequelize, DataTypes) {
  var Column = sequelize.define('Column',  {
    internalName: DataTypes.STRING,
    externalName: DataTypes.STRING,
    color: DataTypes.INTEGER,
    hidden: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        Column.hasMany(models.ColumnValue);
        Column.belongsTo(models.GraphComponent);
      }
    }
  });

  return Column;
};
