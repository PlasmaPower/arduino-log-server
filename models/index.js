'use strict';

var fs = require('fs');
var path = require('path');
var Sequelize = require('sequelize');
var clsNamespace = require('continuation-local-storage').createNamespace;
var sequelizeNamespace = clsNamespace('sequelize');

Sequelize.cls = sequelizeNamespace;
var config = require(__base + '/config.js').database;
var sequelize = new Sequelize(config.database, config.username, config.password, config);
var db = {};

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== 'index.js');
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    if (model) {
      db[model.name] = model;
    }
  });

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.onReady = sequelize.sync();
db.sequelizeCLS = sequelizeNamespace;
db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
