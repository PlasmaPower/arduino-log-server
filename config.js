var path = require('path');
var fs = require('fs');
var os = require('os');
var env = process.env.NODE_ENV || 'development';

if (env === 'development') {
  module.exports = {
    port: 8080,
    password: 1234,
    logging: 'dev',
    database: {
      username: 'root',
      password: null,
      database: 'arduinoLogServer',
      host: '127.0.0.1',
      dialect: 'mysql'
    }
  };
}

if (env === 'testing') {
  module.exports = {
    password: 1234,
    database: {
      username: 'root',
      password: null,
      database: 'arduinoLogServerTest',
      host: 'mysql', // for docker, but you can alias this to localhost with /etc/hosts or equivalent
      dialect: 'mysql',
      logging: false
    }
  };
}

if (env === 'production') {
  module.exports = {
    port: 80,
    password: fs.readFileSync(path.join(__dirname, 'password')),
    logging: 'combined',
    database: {
      username: 'root',
      password: null,
      database: 'arduinoLogServer',
      host: '127.0.0.1',
      dialect: 'mysql',
      logging: false
    }
  };
}
